import React from 'react';
import './App.css';
import "@patternfly/react-core/dist/styles/base.css";
import CustomLogViewer from './components/logviewerfetcher/CustomLogViewer';
import InfoIcon from '@patternfly/react-icons/dist/esm/icons/info-icon';
import QuestionIcon from '@patternfly/react-icons/dist/esm/icons/question-icon';
import { HelperText, HelperTextItem } from '@patternfly/react-core';

import {
  Page,
  PageSection,
  PageSectionVariants,
  Masthead,
  MastheadMain  
} from '@patternfly/react-core';

import {  
  Text,
  TextVariants  
} from '@patternfly/react-core';

export default function App() {   
  
  const Header = (
    <Masthead inset={{ default: 'insetXs' }}>
      <MastheadMain>
        <Text component={TextVariants.h1}>WebEOS Log Viewer</Text>
      </MastheadMain>
    </Masthead>
  );
    
  return (
    <React.Fragment>
      <Page header={Header}>
        <PageSection variant={PageSectionVariants.light}>
          <CustomLogViewer />
        </PageSection>
          <PageSection variant={PageSectionVariants.light}>
          <HelperText>
            <HelperTextItem icon={<InfoIcon />}>To report an issue or ask for assistance, contact us at <a href="https://cern.service-now.com/service-portal?id=service_element&name=PaaS-Web-App">Service Desk - PaaS-Web-App</a>.</HelperTextItem>
          </HelperText>
          <HelperText>
            <HelperTextItem icon={<QuestionIcon />}>For more general questions related to the application, we are also reachable under the <a href="https://mattermost.web.cern.ch/it-dep/channels/openshift">~Openshift</a> channel in Mattermost or in the Central IT Discourse forum at the <a href="https://discourse.web.cern.ch/c/web-hosting-development/openshift">Openshift category</a>.</HelperTextItem>
          </HelperText>
          </PageSection>
      </Page>
    </React.Fragment>
  );
}
