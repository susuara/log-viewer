# Log Viewer application

The log viewer application is composed of several logviewer applications deployed on their corresponding clusters.

See the different folders for futher information about these applications.

## Configure the chart folder

The chart folder, and hence the deployment of the logviewer application, is common for all applications. These applications will be deployed on separate clusters (one per cluster).

Accepted parameters are self-explanatory under the [chart\values.yaml](chart\values.yaml) file, and they are passed through from the logviewer shared sub component under the [okd4-install project](https://gitlab.cern.ch/paas-tools/okd4-install).

For example, the webeos log viewer values are passed from the <https://gitlab.cern.ch/paas-tools/okd4-install/-/blob/master/chart/charts/webeos/templates/webeos-logviewer.yaml> manifest.
