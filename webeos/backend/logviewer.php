<?php

// Uncomment out for Debugging tools
//require './debughelper.php';

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");
header("Content-Type: application/json; charset=UTF-8");

$openshiftAPI = getenv('OPENSHIFT_API_URL');
$esCredentials = getenv('CREDENTIALS');
$urlESBase = getenv('ES_URL_BASE');

// Indexes must much Index Patterns under https://monit-timber-webinfra.cern.ch > Management > Index Patterns
// We need a pattern that satifies both indexes, access and error logs
// e.g. `monit_private_webinfra_logs_webeos-apache`, thus `monit_private_webinfra_logs_webeos-apache-access` and `monit_private_webinfra_logs_webeos-apache-error` will be used.
$indexBase = getenv('ES_INDEX_BASE');

// Fetch parameters from the query and sanitize them
$_POST = filter_var_array(json_decode(file_get_contents('php://input'), true),[
    'action' => [ 'filter' => FILTER_SANITIZE_STRING,
                  'flags' => FILTER_NULL_ON_FAILURE],
    'project' => [ 'filter' => FILTER_SANITIZE_STRING,
                  'flags' => FILTER_NULL_ON_FAILURE],
    'userprovideddirectory' => [ 'filter' => FILTER_SANITIZE_STRING,
                  'flags' => FILTER_NULL_ON_FAILURE],
    'day' => [ 'filter' => FILTER_SANITIZE_STRING,
                  'flags' => FILTER_NULL_ON_FAILURE],
    'startTime' => [ 'filter' => FILTER_SANITIZE_STRING,
                  'flags' => FILTER_NULL_ON_FAILURE],
    'endTime' => [ 'filter' => FILTER_SANITIZE_STRING,
                  'flags' => FILTER_NULL_ON_FAILURE],
    'size' => [ 'filter' => FILTER_SANITIZE_STRING,
                  'flags' => FILTER_NULL_ON_FAILURE]
]);

// In case the user is needed at some point.
// $user = $_SERVER['HTTP_X_FORWARDED_USER'];
$token = isset($_SERVER['HTTP_X_FORWARDED_ACCESS_TOKEN']) ? strval($_SERVER['HTTP_X_FORWARDED_ACCESS_TOKEN']) : null ;
$action = $_POST['action'];
$project = $_POST['project'];
$userprovideddirectory = $_POST['userprovideddirectory'];
$day = $_POST['day'];
$startTime = $_POST['startTime'];
$endTime = $_POST['endTime'];
$size = $_POST['size'];

//## pre-checks
if (is_null($token)){
	echo json_encode(array(
		'status' => 'ERROR',
		'message' => json_encode(array("error" => '❌ Something went wrong. Report this problem to Web Services at https://cern.service-now.com/service-portal/?id=sc_cat_item&name=incident&se=eos-web-hosting'))
  ));
  return;
}

// ensure that project and userprovideddirectory are not empty
if (empty($project) || empty($userprovideddirectory)){
  echo json_encode(array(
		'status' => 'ERROR',
		'message' => json_encode(array("error" => '❌ There are missing query parameters: please use `/?project=xxx&userprovideddirectory=xxx`'))
  ));
  return;
}

// project name check
if (preg_match("/^[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*$/",$project) != 1){
  echo json_encode(array(
    'status' => 'ERROR',
    'message' => json_encode(array("error" => '❌ Query parameter `project` is bad spelling.'))
  ));
  return;
}

if (preg_match("/^[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*$/",$userprovideddirectory) != 1){
  echo json_encode(array(
    'status' => 'ERROR',
    'message' => json_encode(array("error" => '❌ Query parameter `userprovideddirectory` is bad spelling.'))
  ));
  return;
}

// action must be either getlogaccess or getlogerror
if (preg_match("/^getlogaccess|getlogerror$/",$action) != 1 ){
	echo json_encode(array(
		'status' => 'ERROR',
		'message' => json_encode(array("error" => '❌ Action must be in the form of Access or Error.'))
  ));
	return;
}

// A day must be in the form YYYY-MM-DD
if (preg_match("/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])$/",$day) != 1 ){
  echo json_encode(array(
    'status' => 'ERROR',
    'message' => json_encode(array("error" => '❌ The selected date is wrong.'))
  ));
  return;
}

// startTime and endTime must be in the form HH:MM:SS.ssssssZ
if (preg_match("/^(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)((-(\d{2}):(\d{2})|Z)?)$/",$startTime) != 1 ){
  echo json_encode(array(
   'status' => 'ERROR',
   'message' => json_encode(array("error" => '❌ Start time is wrong.'))
 ));
 return;
}
if (preg_match("/^(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)((-(\d{2}):(\d{2})|Z)?)$/",$endTime) != 1 ){
  echo json_encode(array(
   'status' => 'ERROR',
   'message' => json_encode(array("error" => '❌ Start time is wrong.'))
 ));
 return;
}

// size must be in the form of an integer
if (preg_match("/^(\d+)$/",$size) != 1 ){
  echo json_encode(array(
    'status' => 'ERROR',
    'message' => json_encode(array("error" => '❌ Size is wrong.'))
  ));
  return;
}
//## pre-checks

$siteUrlResponse = GetSiteUrl($project,$userprovideddirectory,$token);

if ($siteUrlResponse['status'] === 'OK'){
	$result = GetLogs($action, $day, $size, $siteUrlResponse['message'],$startTime,$endTime);
	echo json_encode(array(
		'status' => 'OK',
		'message' => $result)
	);
} else { //status === 'ERROR'
  echo json_encode(array(
		'status' => 'ERROR',
		'message' => json_encode(array("error" => '❌ ' . $siteUrlResponse['message']))
  ));
}

/**
 * This function will retrieve an array containing the `spec.siteUrl` value from the queried json
 * and the internal status message.
 * It basically peforms the following query in openshift:
 * - `oc get userprovideddirectory/<sitename> -n <project-name> -o json`
 * If a user is not able to fetch the `spec.siteUrl` value, a message from the response will be returned.
 * @param project user's namespace.
 * @param userprovideddirectory user's userprovideddirectory
 * @param token user's token to impersonate the query.
 */
function GetSiteUrl($project, $userprovideddirectory, $token){
	global $openshiftAPI , $token, $project;

  $messageResponse = array();
  
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Accept: application/json',
		'Authorization: Bearer ' . $token
	));

	// URL is in the form of https://openshiftAPI:6443/apis/webeos.webservices.cern.ch/v1alpha1/namespaces/<project>/userprovideddirectories/<userprovideddirectory>
	curl_setopt($ch, CURLOPT_URL, 
	  $openshiftAPI . 
		'/apis/webeos.webservices.cern.ch/v1alpha1/namespaces/' . 
		$project . 
		'/userprovideddirectories/' . 
		$userprovideddirectory);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

	$response = curl_exec($ch);
	curl_close($ch);

	$jsonResponse = json_decode($response);

	// check if site.siteUrl is null or empty
	if (strlen($jsonResponse->{'spec'}->{'siteUrl'}) != 0){
		//User is allowed to see the spec.siteURL
		// Sample output:
		/*
		  ...
			"spec": {
				"eosPath": "/eos/user|project/x/xxxx/www/",
				"globalSettings": {
					"allowAnonymousAccessByDefault": true,
					"archiveSite": false,
					"useDistributedConfigurationFiles": true
				},
				"siteUrl": "sitename.web.cern.ch"
			...
		*/
    $messageResponse = array(
			'status' => 'OK',
			'message' => $jsonResponse->{'spec'}->{'siteUrl'}
		);
	} else {
		//User is unauthorized to see the siteUrl.
		// Output:
		/*{
				"kind": "Status",
				"apiVersion": "v1",
				"metadata": {},
				"status": "Failure",
				"message": "Unauthorized",
				"reason": "Unauthorized",
				"code": 401
			}
		 */
		$messageResponse = array(
			'status' => 'ERROR',
			'message' => 'Message from the server: ' . $jsonResponse->{'message'}
		);
	}

	return $messageResponse;
}

/**
 * Return the hits from ElasticSearch endpoint
 * @param action whether to fetch access logs or error logs. See BuildEsApiBody.
 * @param day day will range the day of logs in the index
 * @param size hits size
 * @param sitename sitename to fetch logs from
 * @param startTime start time of the logs
 * @param endTime end time of the logs
 */
function GetLogs($action, $day, $size, $sitename,$startTime,$endTime) {
	$url = BuildEsApiUrl($action, $day);
	$body = BuildEsApiBody($day,$size, $sitename,$startTime,$endTime);
	$result = CallEsAPI('POST', $url, $body);
		
	return $result;
}

/**
 * This function commutes the endpoint depending on the action a user has selected.
 */
function BuildEsApiUrl($action, $day) {
	global $urlESBase, $indexBase, $day;

	// Query different indexes depending on the parameters
	$urlES_WebEOS = $urlESBase . $indexBase;
  
	switch($action) {
	  case 'getlogaccess':
			$url = $urlES_WebEOS . '-access-' . $day . '/_search';
		break;
		case 'getlogerror':
		  $url = $urlES_WebEOS . '-error-' . $day .'/_search';
		break;
	}

	return $url;
}

/**
 * This function will prepare the variable $body with the proper parameters so that the query will be passed to the ES for fetching expected data.
 * Further info of how to build the query:
 * - https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html
 */
function BuildEsApiBody($day,$size,$sitename,$startTime,$endTime) {

  $body = [
    'size' => $size,
    'sort' => [
      'metadata.timestamp' => [
        'order' => 'asc',
        'unmapped_type' => 'boolean'
      ],
    ],
    'query' => [
      'bool' => [
        'must' => [
          [
            'range' => [
              'metadata.timestamp' => [
                'format' => 'strict_date_optional_time',
                'gte' => $day . 'T' . $startTime,
                'lte' => $day . 'T' . $endTime
              ]
            ]
          ]
        ],
        'filter' => [          
          'bool' => [
            'should' => [
              [
                'match_phrase' => [ 'data.servername' => $sitename]
              ]                
            ],
            'minimum_should_match' => '1'
          ]
        ]
      ]
    ]
  ];

  return $body;
}

/**
 * This function makes the call against Elastic Search.
 * It will return the logs in a json format.
 * @return result json format containing the response from ES.
 */
function CallEsAPI($verb, $url, $data = null) {
	global $esCredentials;
	$ch = curl_init();

	$headers = array(
			'Content-Type: application/json'
	);

	if ($verb === 'POST') {
			$dataStr = json_encode($data);
			$headers[] = 'Content-Length: ' . strlen($dataStr);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $verb);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $dataStr);
	}
	
	curl_setopt($ch, CURLOPT_USERPWD, $esCredentials);

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	
	$result = curl_exec($ch);

	curl_close($ch);

	return $result;
}

?>
