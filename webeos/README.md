# webEOS site log viewer

This application allows users to fetch logs from a specific ES endpoint.

Basically, this application contains 2 containers. One is the OpenShift Oauth2 proxy, and the other the React + PHP application that will handle the requests.

It requires some input, in order to make it run:

- `OPENSHIFT_API_URL`: url of the OpenShift API. In-cluster, it uses to be `https://kubernetes.default.svc.cluster.local`.
- `CREDENTIALS`: These are the credentials relative to the ES endpoint. They must be in the form `user:password`.
- `ES_URL_BASE`: Base url of the ES endpoint. E.g., `https://monit-timber-timber.cern.ch:9203/`.
- `ES_INDEX_BASE`: Index the application will act upon. E.g., `monit_private_webinfra_logs_webeos-apache`.

Both `ES_URL_BASE` and `ES_INDEX_BASE`, together with the selected date in the format `yyyy-mm-dd`, will shape the final endpoint, in the form `ES_URL_BASE``ES_INDEX_BASE``-yyyy-mm-dd/_search`.

## Debug the solution

To debug the solution, we need to deploy separate containers for both the `frontend` and the `backend`. This can be improved at some point by using something similar to `docker-compose`.

### Debug the frontend

```bash
docker run -it --rm -p 3000:3000 --entrypoint=/bin/bash -v "D:\log-viewer\webeos\frontend":/frontend -w /frontend node:17

# npm install in case you don't have the node_modules installed

npm start
```

### Debug the backend

```bash
# Mount the folder where the code is
# Guessing your code is under //d/my-folder/log-viewer
docker run -it --rm -p 8080:8080 -e OPENSHIFT_API_URL='' -e CREDENTIALS='' -e ES_URL_BASE='' -e ES_INDEX_BASE='' --volume "D:\log-viewer\webeos\backend":/home/backend -w /home/backend php:7.4.26-alpine /bin/sh

# Export appropiate environment variables needed
# place over root@7eafe2243cf3:/home/backend#

# OPENSHIFT_API_URL='' # For development purposes, use in the form 'https://api.$CLUSTER_NAME.okd.cern.ch:6443'
# CREDENTIALS='' # Set by <helm_values_file>.logviewer.es.credentials
# ES_URL_BASE='' # Set by <helm_values_file>.logviewer.es.urlBase
# ES_INDEX_BASE='' # Set by <helm_values_file>.logviewer.es.indexBase

# under logviewer.php, change the line from:
# $token = isset($_SERVER['HTTP_X_FORWARDED_ACCESS_TOKEN']) ? $_SERVER['HTTP_X_FORWARDED_ACCESS_TOKEN'] : null;
# To
# $token = isset($_SERVER['HTTP_X_FORWARDED_ACCESS_TOKEN']) ? $_SERVER['HTTP_X_FORWARDED_ACCESS_TOKEN'] : 'a-valid-token'; //(this can be fetched with `oc whoami -t`).
# Remember to NOT COMMIT your token.

# We can opt to run it in background so we can easily interact with the server at the same time.
php -S 0.0.0.0:8080 &
```

Once both they are up and running, just open the browser at `localhost:3000/?project=xxx&userprovideddirectory=xxx` and start playing!

### Debug ES calls

To debug the query directly against ElasticSearch, use the following (it can be under `lxplus8` for instance):

```bash
export CREDENTIALS='' # Set by <helm_secrets_file>.logviewer.es.credentials
export ES_URL_BASE='' # Set by <helm_secrets_file>.logviewer.es.urlBase
export ES_INDEX_BASE='' # Set by <helm_secrets_file>.logviewer.es.indexBase
logaccess='access' # Use `access` or `error`
date='2021-04-26' # Use the pattern YYYY-MM-DD

# You can accomodate the query to the value of $body under the BuildEsApiBody function in webeos\logviewer.php
curl -k --user $CREDENTIALS -X GET "$ES_URL_BASE/$ES_INDEX_BASE-$logaccess-$date/_search" -H 'Content-Type: application/json' -d'
{
  "query" : {
    "query_string" : {
      "query" : "data.servername:mysite.web.cern.ch"
    }
  }
}
'
```
