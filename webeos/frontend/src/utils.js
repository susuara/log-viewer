// This function will prepare the renderization of logs on the LogViewer
function buildData(message,status) {
  const tmpBuffer = [];
  const tmpStatus = status;
  const tmpMessage = JSON.parse(message);
  if (tmpStatus === 'OK'){
      if (tmpMessage.hits){
        var index;
        for (index in tmpMessage.hits.hits) {
            var result = tmpMessage.hits.hits[index];
            tmpBuffer.push(result._source.data.raw + "\n");
        }
      }else{
        tmpBuffer.push("No logs found");
      }
  } else {
    // Any other status different than OK
    // To be improved
    tmpBuffer.push(tmpMessage.error);
  }
  return tmpBuffer;
}

// function to export logs to a txt file.
function downloadLogs(data){
  let filename = "webeos-logs"+Date.now().toString()+".txt";
  let text = data.join(""); // remove ',' (commas from lines)
  let blob = new Blob([text], {type:'text/plain'});
  let link = document.createElement("a");
  link.download = filename;
  //link.innerHTML = "Download File";
  link.href = window.URL.createObjectURL(blob);
  document.body.appendChild(link);
  link.click();
  setTimeout(() => {
      document.body.removeChild(link);
      window.URL.revokeObjectURL(link.href);
  }, 100);
}

export default {buildData,downloadLogs}